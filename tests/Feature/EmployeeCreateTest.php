<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Employee;

class EmployeeCreateTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function first_name_is_required_when_create_a_employee()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());
          //when trying to create a employee with no first name
          $res = $this->post(route('employee.store'), [
               'first_name' => ''
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['first_name']);

          //and no employee is created
          $this->assertCount(0, Employee::get());
     }

     /** @test */
     public function last_name_is_required_when_create_a_employee()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //when trying to create a employee with no  last name
          $res = $this->post(route('employee.store'), [
               'last_name' => ''
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['last_name']);

          //and no employee is created
          $this->assertCount(0, Employee::get());
     }

     /** @test */
     public function employee_email_should_be_valid_email()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //when trying to create a employee with no valid email
          $res = $this->post(route('employee.store'), [
               'email' => 'not_email'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['email']);

          //and no employee is created
          $this->assertCount(0, Employee::get());
     }

     /** @test */
     public function employee_phone_must_be_at_least_6_digits()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //when trying to create a employee with no valid phone
          $res = $this->post(route('employee.store'), [
               'phone' => '1234'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['phone']);

          //and no employee is created
          $this->assertCount(0, Employee::get());
     }

     /** @test */
     public function employee_company_id_should_be_exsisted()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //when trying to create a employee with no valid company
          $res = $this->post(route('employee.store'), [
               'company_id' => '1not_exsist2'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['company_id']);

          //and no employee is created
          $this->assertCount(0, Employee::get());
     }



     /** @test */
     public function admin_can_create_an_employee_with_valid_attributes()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          $attrs = factory('App\Employee')->raw();


          //when trying to create a employee
          $this->post(route('employee.store'), $attrs);


          //and employee is created
          $this->assertDatabaseHas('employees', $attrs);
     }

     /** @test */
     public function admin_redirected_with_success_flash_massage_after_creating()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          $attrs = factory('App\Employee')->raw();


          //when trying to create a employee
          $res = $this->post(route('employee.store'), $attrs);

          $employee = Employee::first();

          //should be redirected
          $res->assertRedirect(route('employee.show', ['id' => $employee->id]));

          // with falsh
          $res->assertSessionHas('success', 'Employee Successfully Created');
     }

     /** @test */
     public function admin_can_show_employee()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          $attrs = factory('App\Employee')->raw();


          //when trying to show employee
          $this->post(route('employee.store'), $attrs);

          $employee = Employee::first();

          //should be able to show employee details
          $this->get(route('employee.show', ['id' => $employee->id]))
               ->assertSee($employee->first_name)
               ->assertSee($employee->last_name)
               ->assertSee($employee->email)
               ->assertSee($employee->phone)
               ->assertSee($employee->company->name);
     }
}
