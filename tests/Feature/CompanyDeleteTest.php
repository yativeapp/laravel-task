<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Company;

class CompanyDeleteTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function admin_can_delete_a_company()
     {
          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();

          $this->delete(route('company.destroy', ['id' => $company->id]));

          $this->assertCount(0, Company::get());
     }


     /** @test */
     public function admin_redirected_with_success_flas_massage_after_creating()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();

          $res = $this->delete(route('company.destroy', ['id' => $company->id]));


          //should be redirected
          $res->assertRedirect(route('company.index'));

          //should be 200 status ok
          $res->assertSessionHas('success', 'Company Successfully Deleted');
     }
}
