<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Employee;

class EmployeeDeleteTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function admin_can_delete_an_employee()
     {
          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and an employee
          $employee = factory('App\Employee')->create();

          $this->delete(route('employee.destroy', ['id' => $employee->id]));

          $this->assertCount(0, Employee::get());
     }


     /** @test */
     public function admin_redirected_with_success_flash_massage_after_deleteing()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //adn a employee
          $employee = factory('App\Employee')->create();

          $res = $this->delete(route('employee.destroy', ['id' => $employee->id]));


          //should be redirected
          $res->assertRedirect(route('employee.index'));

          //should have flash
          $res->assertSessionHas('success', 'Employee Successfully Deleted');
     }
}
