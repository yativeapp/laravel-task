<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Company;

class CompanyCreateTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function company_name_is_required_when_create_a_new_company()
    {

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());
        //when trying to create a company with no name
        $res = $this->post(route('company.store'), [
            'name' => ''
        ]);

        //error should be present 
        $res->assertSessionHasErrors(['name']);

        //and no company is created
        $this->assertCount(0, Company::get());
    }

    /** @test */
    public function company_website_should_be_valid_url()
    {

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        //when trying to create a company with no valid email
        $res = $this->post(route('company.store'), [
            'website' => 'not_url'
        ]);

        //error should be present 
        $res->assertSessionHasErrors(['website']);

        //and no company is created
        $this->assertCount(0, Company::get());
    }

    /** @test */
    public function company_email_should_be_valid_email()
    {

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        //when trying to create a company with no valid email
        $res = $this->post(route('company.store'), [
            'email' => 'not_email'
        ]);

        //error should be present 
        $res->assertSessionHasErrors(['email']);

        //and no company is created
        $this->assertCount(0, Company::get());
    }

    /** @test */
    public function company_logo_must_be_100_100_pixeles()
    {

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        //when trying to create a company with no valid email
        $res = $this->post(route('company.store'), [
            'logo' => UploadedFile::fake()->image('logo.jpg', 50, 50)
        ]);

        //error should be present 
        $res->assertSessionHasErrors(['logo']);

        //and no company is created
        $this->assertCount(0, Company::get());
    }

    /** @test */
    public function admin_can_create_a_company_with_valid_attributes()
    {

        $this->withoutExceptionHandling();

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        $attrs = factory('App\Company')->raw([
            'logo' => UploadedFile::fake()->image('logo.jpg', 100, 100)
        ]);

        //when trying to create a company with valid attrs
        $this->post(route('company.store'), $attrs);


        unset($attrs['logo']);

        //and company is created
        $this->assertDatabaseHas('companies', $attrs);
    }

    /** @test */
    public function admin_redirected_with_success_flash_massage_after_creating()
    {

        $this->withoutExceptionHandling();

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        $attrs = factory('App\Company')->raw([
            'logo' => UploadedFile::fake()->image('logo.jpg', 100, 100)
        ]);


        //when trying to create a company
        $res = $this->post(route('company.store'), $attrs);

        $company = Company::first();

        //should be redirected
        $res->assertRedirect(route('company.show', ['id' => $company->id]));

        //and with flash
        $res->assertSessionHas('success', 'Company Successfully Created');
    }

    /** @test */
    public function admin_can_show_company()
    {

        $this->withoutExceptionHandling();

        //assume we have a signed in admin
        $this->actingAs(factory('App\User')->create());

        $attrs = factory('App\Company')->raw([
            'logo' => UploadedFile::fake()->image('logo.jpg', 100, 100)
        ]);


        //when trying to create a company with  valid attrs
        $res = $this->post(route('company.store'), $attrs);

        $company = Company::first();

        //should be able to show company details
        $this->get(route('company.show', ['id' => $company->id]))
            ->assertSee($company->name)
            ->assertSee($company->logo)
            ->assertSee($company->website)
            ->assertSee($company->email);
    }
}
