<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use App\Company;

class CompanyUpdateTest extends TestCase
{
     use DatabaseTransactions, WithFaker;


     /** @test */
     public function company_name_is_required_when_update_company()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();


          //when trying to update a company with no name
          $res = $this->patch(route('company.update', ['id' => $company->id]), [
               'name' => ''
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['name']);

          //and company not updated
          $this->assertEquals($company->name, $company->fresh()->name);
     }

     /** @test */
     public function company_website_should_be_valid_url()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();


          //when trying to create a company with no valid url
          $res = $this->patch(route('company.update', ['id' => $company->id]), [
               'website' => 'not_url'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['website']);

          //and no company is created
          $this->assertEquals($company->website, $company->fresh()->website);
     }

     /** @test */
     public function company_email_should_be_valid_email()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();


          //when trying to create a company with no valid email
          $res = $this->patch(route('company.update', ['id' => $company->id]), [
               'email' => 'not_email'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['email']);

          //and no company is updated
          $this->assertEquals($company->email, $company->fresh()->email);
     }

     /** @test */
     public function company_logo_must_be_100_100_pixeles()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();

          //when trying to create a company with no valid logo
          $res = $this->patch(route('company.update', ['id' => $company->id]), [
               'logo' => UploadedFile::fake()->image('logo.jpg', 50, 50)
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['logo']);

          //and no company is updated
          $this->assertEquals($company->logo, $company->fresh()->logo);
     }


     /** @test */
     public function admin_redirected_with_success_flash_massage_after_updating()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();


          $attrs = $company->only(['name']);

          //when trying to create a company with no valid email
          $res = $this->patch(route('company.update', ['id' => $company->id]), $attrs);

          //should be redirected
          $res->assertRedirect(route('company.show', ['id' => $company->id]));

          //should have flash
          $res->assertSessionHas('success', 'Company Successfully Updated');
     }


     /** @test */
     public function admin_can_update_a_company()
     {
          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          //and a company
          $company = factory('App\Company')->create();

          $company->name = $this->faker()->name;

          $company->email = $this->faker()->email;

          $company->website = 'anotherwebsite.com';

          $updated = $company->only(['name', 'email', 'website']);

          $this->patch(route('company.update', ['id' => $company->id]), $updated);


          $this->assertCount(1, Company::where($updated)->get());
     }
}
