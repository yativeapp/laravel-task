<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Employee;

class EmployeeUpdateTest extends TestCase
{
     use DatabaseTransactions;

     /** @test */
     public function first_name_is_required_when_updateing_a_employee()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee with no first name
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), [
               'first_name' => ''
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['first_name']);

          //and employee not updated
          $this->assertEquals($employee->name, $employee->fresh()->name);
     }

     /** @test */
     public function last_name_is_required_when_create_a_employee()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee with no last name
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), [
               'last_name' => ''
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['last_name']);

          //and employee not updated
          $this->assertEquals($employee->name, $employee->fresh()->name);
     }

     /** @test */
     public function employee_email_should_be_valid_email()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee with no valid email
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), [
               'email' => 'not_email'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['email']);

          //and employee not updated
          $this->assertEquals($employee->name, $employee->fresh()->name);
     }

     /** @test */
     public function employee_phone_must_be_at_least_6_digits()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee with no valid phone
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), [
               'phone' => '1234'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['phone']);

          //and employee not updated
          $this->assertEquals($employee->name, $employee->fresh()->name);
     }

     /** @test */
     public function employee_company_id_should_be_exsisted()
     {

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee with no valid company
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), [
               'company_id' => '1not_exsist2'
          ]);

          //error should be present 
          $res->assertSessionHasErrors(['company_id']);

          //and employee not updated
          $this->assertEquals($employee->name, $employee->fresh()->name);
     }



     /** @test */
     public function admin_can_create_an_employee_with_valid_attributes()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          $attrs = factory('App\Employee')->raw();


          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee
          $this->patch(route('employee.update', ['id' => $employee->id]), $attrs);

          //and  employee is created
          $this->assertDatabaseHas('employees', $attrs);
     }

     /** @test */
     public function admin_redirected_with_success_flash_massage_after_updating()
     {

          $this->withoutExceptionHandling();

          //assume we have a signed in admin
          $this->actingAs(factory('App\User')->create());

          // and an employee
          $employee = factory('App\Employee')->create();

          //when trying to create a employee
          $res = $this->patch(route('employee.update', ['id' => $employee->id]), $employee->toArray());

          //should be redirected
          $res->assertRedirect(route('employee.show', ['id' => $employee->id]));

          //with flash
          $res->assertSessionHas('success', 'Employee Successfully Updated');
     }
}
