<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables();

        $this->call(UsersTableSeeder::class);
    }


    public function truncateTables()
    {

        $tablesToTruncate = ['users'];

        foreach ($tablesToTruncate as $table) {
            DB::table($table)->truncate();
        }
    }
}
