<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'website', 'logo', 'email'];

    /**
     * return placholder if company has no logo
     *
     * @return image uri
     */
    public function getLogoAttribute($logo)
    {

        return is_null($logo) ? "https://ui-avatars.com/api/?name={$this->name}" : '/storage//' . $logo;
    }


    public function employees()
    {

        return $this->hasMany(Employee::class);
    }
}
