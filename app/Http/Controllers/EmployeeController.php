<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;

use App\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $employees = Employee::with('company')->paginate(10);

        return view('employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new employee.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('employee.create');
    }

    /**
     * Store a newly created employee in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {

        $employee = Employee::create($request->validated());

        return redirect()->route('employee.show', ['id' => $employee->id])
            ->with('success', 'Employee Successfully Created');
    }

    /**
     * Display the employee resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {

        $employee->load('company');

        return view('employee.show', compact('employee'));
    }

    /**
     * Show the form for editing the specified employee.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $employee->load('company');

        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified employee in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {

        $employee->update($request->validated());

        return redirect()->route('employee.show', ['id' => $employee->id])
            ->with('success', 'Employee Successfully Updated');
    }

    /**
     * Remove the specified employee from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Employee::destroy($id);

        return redirect()->route('employee.index')
            ->with('success', 'Employee Successfully Deleted');
    }
}
