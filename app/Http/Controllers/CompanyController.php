<?php

namespace App\Http\Controllers;

use App\Http\Requests\CompanyRequest;
use App\Company;

class CompanyController extends Controller
{
    /**
     * Display a listing of Companies
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $companies = Company::withCount('employees')->paginate(10);

        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new Company
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('company.create');
    }

    /**
     * Store a newly created Company in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {

        $data = $request->validated();

        if ($request->hasFile('logo')) {
            $data['logo']  = $request->logo->store('logos', 'public');
        }

        $company = Company::create($data);

        return redirect()->route('company.show', ['id' => $company->id])
            ->with('success', 'Company Successfully Created');
    }

    /**
     * Display the specified Company.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {

        $employees = $company->employees()->paginate(10);


        return view('company.show', compact('company', 'employees'));
    }

    /**
     * Show the form for editing the specified Comapny.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {

        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified Company in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {

        $data = $request->validated();


        if ($request->hasFile('logo')) {
            $data['logo']  =  $request->logo->store('logos', 'public');
        }

        $company->update($data);

        return redirect()->route('company.show', ['id' => $company->id])
            ->with('success', 'Company Successfully Updated');
    }

    /**
     * Remove the specified company from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Company::destroy($id);

        return redirect()->route('company.index')
            ->with('success', 'Company Successfully Deleted');
    }
}
