@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <strong>Company:</strong>
            <a href="{{ route('company.show' , ['id' => $employee->company->id]) }}" target="_blank">
                {{$employee->company->name}}
            </a>

        </div>
        <div class="card-body">
            <p>
                <strong>First Name:</strong>
                {{ $employee->first_name }}
            </p>
            <p>
                <strong>Last Name:</strong>
                {{ $employee->last_name }}
            </p>
            <p>
                <strong>Email:</strong>
                {{ $employee->email }}
            </p>
            <p>
                <strong>Phone:</strong>
                {{ $employee->phone }}
            </p>
        </div>
    </div>

</div>
@endsection
