@extends('layouts.app')

@section('content')
@include('partials._employee-form' , ['employee' => new \App\Employee])
@endsection
