@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card" style="margin-bottom:2em;">
        <div class="card-header">
            <img src="{{ $company->logo }}" alt="{{ $company->name }}" class="img-thumbnail">
            <p>
                <h2>{{ $company->name }}</h2>
            </p>

        </div>
        <div class="card-body">
            <p>
                <strong>Email:</strong>
                {{ $company->email }}
            </p>
            <p>
                <strong>Website:</strong>
                <a href="{{$company->website}}">{{$company->website}}</a>
            </p>
        </div>
    </div>


    @include('partials._employees-show' , ['company' => $company])
</div>
@endsection
