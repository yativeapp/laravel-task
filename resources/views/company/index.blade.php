@extends('layouts.app')

@section('content')
<div class="container">
    @include('partials._delete-modal')


    <a href="{{ route('company.create') }}" class="btn btn-primary float-right" style="margin-bottom:2em;">New Company</a>

    <table class="table  table-striped table-hover">
        <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Logo</th>
                <th scope="col"># Employees</th>
                <th scope="col">Name</th>
                <th scope="col">Email</th>
                <th scope="col">Website</th>
                <th scope="col" class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($companies as $company)
            <tr>
                <td>{{ $company->id }}</td>
                <td> <img src="{{ $company->logo }}" alt="{{ $company->name }}" class="img-thumbnail"> </td>
                <td>{{ $company->employees_count }}</td>
                <td>{{ $company->name }}</td>
                <td>{{ $company->email }}</td>
                <td>
                    <a href="{{ $company->website }}" target="_blank">{{ $company->website }}</a>
                </td>
                <td class="text-right">
                    <div class="btn-group ">
                        <a href="{{ route('company.show' , ['id' => $company->id]) }}" class="btn btn-secondary">
                            Show Full
                        </a>
                        <a href="{{ route('company.edit' , ['id' => $company->id]) }}" class="btn btn-info">
                            Edit
                        </a>

                        <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"
                            data-url="{{ route('company.destroy' , ['id' => $company->id]) }}">Delete</button>


                    </div>
                </td>

            </tr>
            @empty
            <tr>
                <td class="text-center" colspan="6">No Companies</td>
            </tr>
            @endforelse
        </tbody>
    </table>
    {{ $companies->links() }}
</div>
@endsection
