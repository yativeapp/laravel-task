<div aria-live="assertive" aria-atomic="true" style="position: relative; min-height: 200px;">
    <div class="toast" style="position: absolute; top: 0; right: 0;">
        @if(session()->has('success'))
        <div class="toast-body">
            {{ session()->get('success')}}
        </div>
        @endif
    </div>
</div>

@if(session()->has('success'))
<script>
    window.addEventListener('load', function () {
        $('.toast').toast({
            delay: 5000
        });
        $('.toast').toast('show')
    });

</script>
@endif
