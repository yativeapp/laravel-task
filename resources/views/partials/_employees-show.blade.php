<a href="{{ route('employee.create') }}{{ isset($company) ? '?c=' . $company->id : '' }}" class="btn btn-primary float-right"
    style="margin-bottom:2em;">New
    employee</a>

<table class="table  table-striped table-hover">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            @if(!isset($company))
            <th scope="col">Company</th>
            @endif
            <th scope="col" class="text-center">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse($employees as $employee)
        <tr>
            <td>{{ $employee->id }}</td>
            <td>{{ $employee->first_name }}</td>
            <td>{{ $employee->last_name }}</td>
            <td>{{ $employee->email }}</td>
            <td>{{ $employee->phone }}</td>
            @if(!isset($company))
            <td> <a href="
    {{ route('company.show' , ['id' => $employee->company->id]) }}">
                    {{$employee->company->name}}
                </a></td>
            @endif
            <td class="
    text-right">
                <div class="btn-group ">
                    <a href="{{ route('employee.edit' , ['id' => $employee->id]) }}" class="btn btn-info">
                        Edit
                    </a>

                    <button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete"
                        data-url="{{ route('employee.destroy' , ['id' => $employee->id]) }}">Delete</button>
                </div>
            </td>

        </tr>
        @empty
        <tr>
            <td class="text-center" colspan="6">No Employees</td>
        </tr>
        @endforelse
    </tbody>
</table>

{{ $employees->links() }}

@include('partials._delete-modal')
