<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form action="" method="POST" class="modal-dialog modal-dialog-centered " role="document">
        @csrf
        @method('DELETE')

        <input type="hidden">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are You Sure?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                You can't restore After Delete
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Delete</button>
            </div>
        </div>
    </form>
</div>

<script>
    window.addEventListener('load', function () {

        $('#modal-delete').on('show.bs.modal', function (event) {

            $(this).find('form').attr('action', $(event.relatedTarget).data('url'));

        })
    });

</script>
