<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> {{ $company->exists ? __('Update') :__('Create Company') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ $company->exists ? route('company.update' , ['id' => $company->id]) :route('company.store') }}"
                        enctype="multipart/form-data">
                        @csrf

                        @if($company->exists)
                        @method('PATCH')
                        @endif

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                    name="name" value="{{ old('name') ?? $company->name }}">

                                @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ old('email') ?? $company->email }}">


                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="logo" class="col-md-4 col-form-label text-md-right">{{ __('Logo') }}</label>

                            <div class="col-md-6">
                                <div class="custom-file">
                                    <input type="file" name="logo" class="custom-file-input {{ $errors->has('logo') ? ' is-invalid' : '' }}"
                                        id="logo" accept="image/*">
                                    <label class="custom-file-label" for="logo">
                                        {{ $company->exsist ? 'Choose Logo' : $company->name }}
                                    </label>
                                    @if ($errors->has('logo'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Website') }}</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="website">
                                            http://
                                        </span>
                                    </div>
                                    <input type="text" name="website" class="form-control {{ $errors->has('website') ? ' is-invalid' : '' }}"
                                        id="website" val="{{ old('website') ?? $company->website }}">

                                    @if ($errors->has('website'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                    @endif
                                </div>


                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ $company->exists ? __('Update') :__('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<script defer>
    window.addEventListener('load', function () {
        $('#logo').on('change', function () {
            //get the file
            var file = $(this)[0].files[0];

            if (file) {
                $(this).next('.custom-file-label').html(file.name);
            }

        })
    });

</script>
